#!/usr/bin/env python3
"""Builds packages"""
import sys
from pathlib import Path
from prebuilder import *
from prebuilder.systems import Make
from prebuilder.buildPipeline import BuildRecipy, BuildPipeline
from prebuilder.repoPipeline import RepoPipelineMeta
from ClassDictMeta import ClassDictMeta
from prebuilder.fetchers import GitRepoFetcher
from fsutilz import movetree, copytree

from prebuilder.core.Package import PackageMetadata
from prebuilder.distros.debian import Debian
from prebuilder.importers.debhelper import parseDebhelperDebianDir


class build(metaclass=RepoPipelineMeta):
	"""It's {maintainerName}'s repo for {repoKind} of openSeaChest tools by Seagate™."""
	
	DISTROS = (Debian,)

	def openSeaChest():
		name = "openseachest"
		repoURI = "https://github.com/Seagate/openSeaChest"
		
		class cfg(metaclass=ClassDictMeta):
			descriptionShort = "A set of tools to do low-level stuff to storage devices. Especially to Seagate™ ones."
			descriptionLong = """May be considered an analogue of hdparm, but the tools are splitted across different utilities. I have not done feature-by-feature comparison."""
			section = "devel"
			homepage = repoURI
			# "license": "MPL-2.0"
			provides = (name,)
		
		def installRule(sourceDir, buildDir, pkg, gnuDirs):
			buildDir = sourceDir
			rootDir = buildDir / ".." / ".."
			copytree(buildDir / "openseachest_exes", pkg.nest(gnuDirs.bin))
			copytree(rootDir / "docs" / "man", pkg.nest(gnuDirs.man))
		
		buildRecipy = BuildRecipy(Make, GitRepoFetcher(repoURI, submodules=True), buildOptions = {}, subdir="Make/gcc/", configureScript=None, installRule=installRule, makeArgs=("release",), useKati=False)
		metadata = PackageMetadata(name, **cfg)
		
		return BuildPipeline(buildRecipy, ((Debian, metadata),))



if __name__ == "__main__":
	build()
